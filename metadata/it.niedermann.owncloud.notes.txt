Categories:Writing,Internet
License:GPLv2+
Web Site:
Source Code:https://github.com/stefan-niedermann/OwnCloud-Notes
Issue Tracker:https://github.com/stefan-niedermann/OwnCloud-Notes/issues

Auto Name:ownCloud Notes
Summary:Client for ownCloud Notes App
Description:
Companion app to ownCloud Notes.

Features

* List, Create, Edit, Share and Delete Notes
* Share Text and Links as new Note into the App
* Bulk Delete
* Render MarkDown (using Bypass)
* English, German, Russian, Armenian and Serbian UI

Requirements

* ownCloud instance running
* ownCloud Notes app enabled

Alternatives

If you dislike this app and you are looking for alternatives: Have a look at
[[org.aykit.MyOwnNotes]].
.

Repo Type:git
Repo:https://github.com/stefan-niedermann/OwnCloud-Notes.git

Build:0.4.0,4
    commit=v0.4.0
    subdir=app
    gradle=yes

Build:0.5.0,5
    commit=v0.5.0
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:0.5.0
Current Version Code:5
